package com.wot.ionic.unitymanager;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.content.Intent;
import com.wot.ammazza.UnityPlayerActivity;

/**
 * This class echoes a string called from JavaScript.
 */
public class UnityManager extends CordovaPlugin {

    public static final String LOG_TAG = "unityLog";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        // Demo Code
        // if (action.equals("coolMethod")) {
        //     String message = args.getString(0);
        //     this.coolMethod(message, callbackContext);
        //     return true;
        // }
        if ("startUnity".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    Intent intent = new Intent(cordova.getActivity(), UnityPlayerActivity.class);
                    cordova.getActivity().startActivity(intent);
                    callbackContext.success("It\'s Working DArshit.!");
                }
            });
            // callbackContext.success("It\'s Working.!");
            return true; 
        }
        return false; // Returning false results in a "MethodNotFound" error.
    }

    @Override
    public void onDestroy() {
       Log.d(LOG_TAG, "onDestroy Method called.!");
    }
    
    // Demo Method
    // private void coolMethod(String message, CallbackContext callbackContext) {
    //     if (message != null && message.length() > 0) {
    //         callbackContext.success(message);
    //     } else {
    //         callbackContext.error("Expected one non-empty string argument.");
    //     }
    // }
}
