var UnityManagerLoader = function(require, exports, module) {
var exec = require('cordova/exec');

function UnityManagerHandler() {}

UnityManagerHandler.prototype.startUnity = function(success, failure, timeOffset) {
    exec(success, failure, 'UnityManager', 'startUnity', []);
};


var unityManagerHandler = new UnityManagerHandler();
    module.exports = unityManagerHandler;
};

UnityManagerLoader(require, exports, module);

cordova.define("cordova/plugin/UnityManager", UnityManagerLoader);
  